class Controller
{
	constructor()
	{
		Controller.instance = this;

		this.keys = [];

		document.addEventListener('keydown', this.keydownHandler.bind(this));
		document.addEventListener('keyup', this.keyupHandler.bind(this));
	}

	keydownHandler(e)
	{
		var key = e.key.toLowerCase();
		var index = this.keys.indexOf(key);

		if (index < 0) {
			this.keys.push(key);
		}
	}

	keyupHandler(e)
	{
		var key = e.key.toLowerCase();
		var index = this.keys.indexOf(key);

		if (index >= 0) {
			this.keys.splice(index, 1);
		}
	}

	isDown(key)
	{
		return this.keys.indexOf(key) >= 0;
	}
}
