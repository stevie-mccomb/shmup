class Sprite
{
	constructor(entity, src, options)
	{
		if (!options) options = {};

		this.entity = entity;

		this.image = new Image();
		this.image.src = 'img/sprites/' + src;

		this.tickCount = 0;
		this.ticksPerFrame = options.ticksPerFrame || 0;
		this.frameIndex = options.frameIndex || 0;
		this.animationIndex = options.animationIndex || 0;
		this.framesPerAnimation = options.framesPerAnimation || 1;

		this.loop = options.loop !== undefined ? options.loop : true;
		this.paused = options.paused !== undefined ? options.paused : false;

		this.opacity = options.opacity || 1;
	}

	update(deltaTime)
	{
		if (!this.paused) {
			++this.tickCount;

			if (this.tickCount >= this.ticksPerFrame) {
				this.tickCount = 0;

				++this.frameIndex;

				if (this.frameIndex >= this.framesPerAnimation) {
					if (this.loop) {
						this.frameIndex = 0;
					} else {
						--this.frameIndex;
						this.paused = true;
					}
				}
			}
		}

		this.subX = this.frameIndex * this.entity.width;
		this.subY = this.animationIndex * this.entity.height;
	}

	redraw(deltaTime)
	{
		if (this.opacity !== 1) Stage.instance.context.globalAlpha = this.opacity;
		Stage.instance.context.drawImage(this.image, this.subX, this.subY, this.entity.width, this.entity.height, this.entity.x, this.entity.y, this.entity.width, this.entity.height);
		if (this.opacity !== 1) Stage.instance.context.globalAlpha = 1;
	}
}
