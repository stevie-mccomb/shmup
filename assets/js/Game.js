class Game
{
	constructor()
	{
		Game.instance = this;

		this.element = document.createElement('game');
		document.body.appendChild(this.element);

		this.scoreBoard = document.createElement('score');
		this.element.appendChild(this.scoreBoard);

		this.lastUpdated = 0;

		this.score = 0;

		new Controller();
		new Stage();
		new StarField('stars_1.png', 1);
		new StarField('stars_2.png', 1.15);
		new Hero();

		new EnemySpawn();

		requestAnimationFrame(this.loop.bind(this));
	}

	loop(timestamp)
	{
		var deltaTime = timestamp - this.lastUpdated;

		if (!this.paused) {
			this.update(deltaTime);
			this.redraw(deltaTime);
		}

		this.lastUpdated = timestamp;

		requestAnimationFrame(this.loop.bind(this));
	}

	update(deltaTime)
	{
		Stage.instance.update(deltaTime);

		for (var i = 0; i < GameObject.instances.length; ++i) {
			GameObject.instances[i].update(deltaTime);
		}
	}

	redraw(deltaTime)
	{
		Stage.instance.redraw(deltaTime);

		for (var i = 0; i < GameObject.instances.length; ++i) {
			GameObject.instances[i].redraw(deltaTime);
		}
	}

	gameOver()
	{
		this.gameOverScreen = document.createElement('gameover');
		this.gameOverTitle = document.createElement('h1');
		this.restartButton = document.createElement('button');

		this.element.appendChild(this.gameOverScreen);
		this.gameOverScreen.appendChild(this.gameOverTitle);
		this.gameOverScreen.appendChild(this.restartButton);

		this.gameOverTitle.innerText = 'Game Over!';
		this.restartButton.innerText = 'Restart';

		this.restartButton.addEventListener('click', this.restart.bind(this));
	}

	restart()
	{
		this.score = 0;

		new Hero();

		for (var i = Enemy.instances.length - 1; i >= 0; i--) {
			Enemy.instances[i].destroy();
		}

		if (this.gameOverScreen.parentNode) {
			this.gameOverScreen.parentNode.removeChild(this.gameOverScreen);
		}
	}

	get score()
	{
		return this._score;
	}

	set score(value)
	{
		this._score = value;

		this.scoreBoard.innerText = 'Score: ' + value;
	}
}

document.addEventListener('DOMContentLoaded', function () {
	new Game();
});

requestAnimationFrame = requestAnimationFrame || webkitRequestAnimationFrame || mozRequestAnimationFrame || function (callback) { setTimeout(callback, 1000 / 60); }
