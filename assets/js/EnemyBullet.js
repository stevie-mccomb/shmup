class EnemyBullet extends GameObject
{
	constructor(owner)
	{
		super();

		EnemyBullet.instances.push(this);

		this.owner = owner;

		this.velocity = 8;

		this.width = 9;
		this.height = 24;
		this.x = this.owner.x + (this.owner.width / 2) - (this.width / 2);
		this.top = this.owner.bottom;

		this.sprite = new Sprite(this, 'enemyBullet.png');
	}

	update(deltaTime)
	{
		super.update(deltaTime);

		this.y += this.velocity;

		if (this.top >= Stage.instance.bottom) {
			this.destroy();
		}
	}

	destroy()
	{
		super.destroy();

		var index = EnemyBullet.instances.indexOf(this);

		if (index >= 0) {
			EnemyBullet.instances.splice(index, 1);
		}
	}
}

EnemyBullet.instances = [];
