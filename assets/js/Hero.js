class Hero extends GameObject
{
	constructor()
	{
		super();

		Hero.instance = this;

		this.width = 64;
		this.height = 64;
		this.x = (Stage.instance.width / 2) - (this.width / 2);
		this.y = Stage.instance.height - this.height - 32;

		this.lives = 3;
		this.respawnTick = 0;
		this.respawnRate = 124;
		this.deathless = false;
		this.flickerTick = 0;
		this.flickerRate = 16;

		this.reloadTick = 0;
		this.fireRate = 31;
		this.canFire = true;

		this.maxSpeed = 8;
		this.acceleration = 0.5;
		this.horizontalSpeed = 0;
		this.verticalSpeed = 0;

		this.sprite = new Sprite(this, 'hero.png', {
			loop: false,
			ticksPerFrame: 6
		});
	}

	update(deltaTime)
	{
		super.update(deltaTime);

		if (this.deathless) {
			this.flicker(deltaTime);
			this.respawn(deltaTime);
		}

		this.reload(deltaTime);
		this.fire(deltaTime);
		this.updateSpeed(deltaTime);
		this.updateAnims(deltaTime);
		this.move(deltaTime);
		this.decay(deltaTime);
		this.checkCollisions();
		this.checkBounds();
	}

	destroy()
	{
		if (this.deathless) return;

		--this.lives;

		if (this.lives < 0) {
			super.destroy();

			Hero.instance = null;

			Game.instance.gameOver();
		} else {
			this.deathless = true;

			Game.instance.score -= 50;
		}
	}

	flicker(deltaTime)
	{
		++this.flickerTick;

		if (this.flickerTick >= this.flickerRate) {
			this.flickerTick = 0;

			if (this.sprite.opacity === 1) {
				this.sprite.opacity = 0.5;
			} else {
				this.sprite.opacity = 1;
			}
		}
	}

	respawn(deltaTime)
	{
		++this.respawnTick;

		if (this.respawnTick >= this.respawnRate) {
			this.respawnTick = 0;

			this.deathless = false;
			this.sprite.opacity = 1;
		}
	}

	reload(deltaTime)
	{
		if (this.canFire) return;

		++this.reloadTick;

		if (this.reloadTick >= this.fireRate) {
			this.reloadTick = 0;
			this.canFire = true;
		}
	}

	fire(deltaTime)
	{
		if (!this.canFire) return;

		if (Controller.instance.isDown(' ') || Controller.instance.isDown('spacebar')) {
			new Bullet();
			this.canFire = false;
		}
	}

	updateSpeed(deltaTime)
	{
		if (Controller.instance.isDown('w') && this.verticalSpeed > -this.maxSpeed) {
			this.verticalSpeed -= this.acceleration;
		}

		if (Controller.instance.isDown('a') && this.horizontalSpeed > -this.maxSpeed) {
			this.horizontalSpeed -= this.acceleration;
		}

		if (Controller.instance.isDown('s') && this.verticalSpeed < this.maxSpeed) {
			this.verticalSpeed += this.acceleration;
		}

		if (Controller.instance.isDown('d') && this.horizontalSpeed < this.maxSpeed) {
			this.horizontalSpeed += this.acceleration;
		}
	}

	updateAnims(deltaTime)
	{
		if (this.horizontalSpeed < 0 && this.sprite.animationIndex !== 1) {
			this.sprite.animationIndex = 1;
			this.sprite.frameIndex = 0;
			this.sprite.framesPerAnimation = 2;
			this.sprite.paused = false;
		} else if (this.horizontalSpeed > 0 && this.sprite.animationIndex !== 2) {
			this.sprite.animationIndex = 2;
			this.sprite.frameIndex = 0;
			this.sprite.framesPerAnimation = 2;
			this.sprite.paused = false;
		} else if (this.horizontalSpeed === 0) {
			this.sprite.animationIndex = 0;
			this.sprite.frameIndex = 0;
			this.sprite.framesPerAnimation = 1;
		}
	}

	move(deltaTime)
	{
		this.x += this.horizontalSpeed;
		this.y += this.verticalSpeed;
	}

	decay(deltaTime)
	{
		if (!Controller.instance.isDown('w') && !Controller.instance.isDown('s')) {
			if (this.verticalSpeed > 0) {
				this.verticalSpeed -= this.acceleration;

				if (this.verticalSpeed < this.acceleration) {
					this.verticalSpeed = 0;
				}
			} else if (this.verticalSpeed < 0) {
				this.verticalSpeed += this.acceleration;

				if (this.verticalSpeed > this.acceleration) {
					this.verticalSpeed = 0;
				}
			}
		}

		if (!Controller.instance.isDown('a') && !Controller.instance.isDown('d')) {
			if (this.horizontalSpeed > 0) {
				this.horizontalSpeed -= this.acceleration;

				if (this.horizontalSpeed < this.acceleration) {
					this.horizontalSpeed = 0;
				}
			} else if (this.horizontalSpeed < 0) {
				this.horizontalSpeed += this.acceleration;

				if (this.horizontalSpeed > this.acceleration) {
					this.horizontalSpeed = 0;
				}
			}
		}
	}

	checkCollisions()
	{
		for (var i = 0; i < Enemy.instances.length; ++i) {
			if (this.isColliding(Enemy.instances[i])) {
				Enemy.instances[i].destroy();
				this.destroy();
			}
		}

		for (var i = 0; i < EnemyBullet.instances.length; ++i) {
			if (this.isColliding(EnemyBullet.instances[i])) {
				this.destroy();
			}
		}
	}

	checkBounds()
	{
		if (this.top < Stage.instance.top) {
			this.verticalSpeed = 0;
			this.top = Stage.instance.top;
		}

		if (this.left < Stage.instance.left) {
			this.horizontalSpeed = 0;
			this.left = Stage.instance.left;
		}

		if (this.bottom > Stage.instance.bottom) {
			this.verticalSpeed = 0;
			this.bottom = Stage.instance.bottom;
		}

		if (this.right > Stage.instance.right) {
			this.horizontalSpeed = 0;
			this.right = Stage.instance.right;
		}
	}

	get lives()
	{
		return this._lives;
	}

	set lives(value)
	{
		for (var i = Life.instances.length - 1; i >= 0; --i) {
			Life.instances[i].destroy();
		}

		for (var i = 0; i < value; ++i) {
			new Life();
		}

		this._lives = value;
	}
}
