class EnemySpawn extends GameObject
{
	constructor()
	{
		super();

		EnemySpawn.instance = this;

		this.tickCount = 0;
		this.spawnRate = 124;
		this.tickTrigger = Math.ceil(Math.random() * this.spawnRate);
	}

	update(deltaTime)
	{
		++this.tickCount;

		if (this.tickCount >= this.tickTrigger) {
			new Enemy();

			this.tickCount = 0;
			this.tickTrigger = Math.ceil(Math.random() * this.spawnRate);
		}
	}
}
