class Life extends GameObject
{
	constructor()
	{
		super();

		Life.instances.push(this);

		this.width = 19;
		this.height = 22;

		this.x = Stage.instance.right - ((this.width + 16) * Life.instances.length);
		this.y = 16;

		this.sprite = new Sprite(this, 'life.png');
	}

	destroy()
	{
		super.destroy();

		var index = Life.instances.indexOf(this);

		if (index >= 0) {
			Life.instances.splice(index, 1);
		}
	}
}

Life.instances = [];
