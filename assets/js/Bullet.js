class Bullet extends GameObject
{
	constructor()
	{
		super();

		Bullet.instances.push(this);

		this.velocity = 8;

		this.width = 9;
		this.height = 24;
		this.x = Hero.instance.x + (Hero.instance.width / 2) - (this.width / 2);
		this.y = Hero.instance.y - this.height + this.velocity;

		this.sprite = new Sprite(this, 'bullet.png');
	}

	update(deltaTime)
	{
		super.update(deltaTime);

		this.y -= this.velocity;

		if (this.bottom < Stage.instance.top) {
			this.destroy();
		}
	}

	destroy()
	{
		super.destroy();

		var index = Bullet.instances.indexOf(this);

		if (index >= 0) {
			Bullet.instances.splice(index, 1);
		}
	}
}

Bullet.instances = [];
