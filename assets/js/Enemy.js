class Enemy extends GameObject
{
	constructor()
	{
		super();

		Enemy.instances.push(this);

		this.width = 64;
		this.height = 64;
		this.x = Math.floor(Math.random() * (Stage.instance.width - this.width));
		this.bottom = Stage.instance.top;

		this.reloadTick = 0;
		this.fireRate = 54;
		this.canFire = true;

		this.speed = 4;

		this.value = 10;

		this.sprite = new Sprite(this, 'enemy.png');
	}

	update(deltaTime)
	{
		super.update(deltaTime);

		this.reload(deltaTime);
		this.fire(deltaTime);

		this.y += this.speed;

		this.checkCollisions();

		if (this.top >= Stage.instance.bottom) {
			this.destroy();
		}
	}

	destroy()
	{
		super.destroy();

		var index = Enemy.instances.indexOf(this);

		if (index >= 0) {
			Enemy.instances.splice(index, 1);
		}
	}

	reload(deltaTime)
	{
		if (this.canFire) return;

		++this.reloadTick;

		if (this.reloadTick >= this.fireRate) {
			this.reloadTick = 0;
			this.canFire = true;
		}
	}

	fire(deltaTime)
	{
		if (!this.canFire) return;

		new EnemyBullet(this);
		this.canFire = false;
	}

	checkCollisions()
	{
		for (var i = 0; i < Bullet.instances.length; ++i) {
			if (this.isColliding(Bullet.instances[i])) {
				Game.instance.score += this.value;

				this.destroy();
			}
		}
	}
}

Enemy.instances = [];
