class StarField extends GameObject
{
	constructor(src, speed)
	{
		super();

		this.width = Stage.instance.width;
		this.height = Stage.instance.height * 2;
		this.bottom = Stage.instance.bottom;

		this.speed = speed;

		this.sprite = new Sprite(this, src, {
			loop: false,
			paused: true
		});
	}

	update(deltaTime)
	{
		super.update(deltaTime);

		this.y += this.speed;

		if (this.top >= Stage.instance.top) {
			this.bottom = Stage.instance.bottom;
		}
	}
}
