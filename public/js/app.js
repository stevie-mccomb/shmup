'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Controller = function () {
	function Controller() {
		_classCallCheck(this, Controller);

		Controller.instance = this;

		this.keys = [];

		document.addEventListener('keydown', this.keydownHandler.bind(this));
		document.addEventListener('keyup', this.keyupHandler.bind(this));
	}

	_createClass(Controller, [{
		key: 'keydownHandler',
		value: function keydownHandler(e) {
			var key = e.key.toLowerCase();
			var index = this.keys.indexOf(key);

			if (index < 0) {
				this.keys.push(key);
			}
		}
	}, {
		key: 'keyupHandler',
		value: function keyupHandler(e) {
			var key = e.key.toLowerCase();
			var index = this.keys.indexOf(key);

			if (index >= 0) {
				this.keys.splice(index, 1);
			}
		}
	}, {
		key: 'isDown',
		value: function isDown(key) {
			return this.keys.indexOf(key) >= 0;
		}
	}]);

	return Controller;
}();
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var GameObject = function () {
	function GameObject() {
		_classCallCheck(this, GameObject);

		GameObject.instances.push(this);

		this.width = 0;
		this.height = 0;
		this.x = 0;
		this.y = 0;
	}

	_createClass(GameObject, [{
		key: "update",
		value: function update(deltaTime) {
			//
		}
	}, {
		key: "redraw",
		value: function redraw(deltaTime) {
			if (this.sprite) {
				this.sprite.update(deltaTime);
				this.sprite.redraw(deltaTime);
			}
		}
	}, {
		key: "destroy",
		value: function destroy() {
			var index = GameObject.instances.indexOf(this);

			if (index >= 0) {
				GameObject.instances.splice(index, 1);
			}
		}
	}, {
		key: "isColliding",
		value: function isColliding(obj) {
			return this.top <= obj.bottom && this.left <= obj.right && this.bottom >= obj.top && this.right >= obj.left;
		}
	}, {
		key: "top",
		get: function get() {
			return this.y;
		},
		set: function set(value) {
			this.y = value;
		}
	}, {
		key: "left",
		get: function get() {
			return this.x;
		},
		set: function set(value) {
			this.x = value;
		}
	}, {
		key: "bottom",
		get: function get() {
			return this.top + this.height;
		},
		set: function set(value) {
			this.y = value - this.height;
		}
	}, {
		key: "right",
		get: function get() {
			return this.left + this.width;
		},
		set: function set(value) {
			this.x = value - this.width;
		}
	}]);

	return GameObject;
}();

GameObject.instances = [];
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Sprite = function () {
	function Sprite(entity, src, options) {
		_classCallCheck(this, Sprite);

		if (!options) options = {};

		this.entity = entity;

		this.image = new Image();
		this.image.src = 'img/sprites/' + src;

		this.tickCount = 0;
		this.ticksPerFrame = options.ticksPerFrame || 0;
		this.frameIndex = options.frameIndex || 0;
		this.animationIndex = options.animationIndex || 0;
		this.framesPerAnimation = options.framesPerAnimation || 1;

		this.loop = options.loop !== undefined ? options.loop : true;
		this.paused = options.paused !== undefined ? options.paused : false;

		this.opacity = options.opacity || 1;
	}

	_createClass(Sprite, [{
		key: 'update',
		value: function update(deltaTime) {
			if (!this.paused) {
				++this.tickCount;

				if (this.tickCount >= this.ticksPerFrame) {
					this.tickCount = 0;

					++this.frameIndex;

					if (this.frameIndex >= this.framesPerAnimation) {
						if (this.loop) {
							this.frameIndex = 0;
						} else {
							--this.frameIndex;
							this.paused = true;
						}
					}
				}
			}

			this.subX = this.frameIndex * this.entity.width;
			this.subY = this.animationIndex * this.entity.height;
		}
	}, {
		key: 'redraw',
		value: function redraw(deltaTime) {
			if (this.opacity !== 1) Stage.instance.context.globalAlpha = this.opacity;
			Stage.instance.context.drawImage(this.image, this.subX, this.subY, this.entity.width, this.entity.height, this.entity.x, this.entity.y, this.entity.width, this.entity.height);
			if (this.opacity !== 1) Stage.instance.context.globalAlpha = 1;
		}
	}]);

	return Sprite;
}();
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var StarField = function (_GameObject) {
	_inherits(StarField, _GameObject);

	function StarField(src, speed) {
		_classCallCheck(this, StarField);

		var _this = _possibleConstructorReturn(this, (StarField.__proto__ || Object.getPrototypeOf(StarField)).call(this));

		_this.width = Stage.instance.width;
		_this.height = Stage.instance.height * 2;
		_this.bottom = Stage.instance.bottom;

		_this.speed = speed;

		_this.sprite = new Sprite(_this, src, {
			loop: false,
			paused: true
		});
		return _this;
	}

	_createClass(StarField, [{
		key: "update",
		value: function update(deltaTime) {
			_get(StarField.prototype.__proto__ || Object.getPrototypeOf(StarField.prototype), "update", this).call(this, deltaTime);

			this.y += this.speed;

			if (this.top >= Stage.instance.top) {
				this.bottom = Stage.instance.bottom;
			}
		}
	}]);

	return StarField;
}(GameObject);
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var EnemyBullet = function (_GameObject) {
		_inherits(EnemyBullet, _GameObject);

		function EnemyBullet(owner) {
				_classCallCheck(this, EnemyBullet);

				var _this = _possibleConstructorReturn(this, (EnemyBullet.__proto__ || Object.getPrototypeOf(EnemyBullet)).call(this));

				EnemyBullet.instances.push(_this);

				_this.owner = owner;

				_this.velocity = 8;

				_this.width = 9;
				_this.height = 24;
				_this.x = _this.owner.x + _this.owner.width / 2 - _this.width / 2;
				_this.top = _this.owner.bottom;

				_this.sprite = new Sprite(_this, 'enemyBullet.png');
				return _this;
		}

		_createClass(EnemyBullet, [{
				key: 'update',
				value: function update(deltaTime) {
						_get(EnemyBullet.prototype.__proto__ || Object.getPrototypeOf(EnemyBullet.prototype), 'update', this).call(this, deltaTime);

						this.y += this.velocity;

						if (this.top >= Stage.instance.bottom) {
								this.destroy();
						}
				}
		}, {
				key: 'destroy',
				value: function destroy() {
						_get(EnemyBullet.prototype.__proto__ || Object.getPrototypeOf(EnemyBullet.prototype), 'destroy', this).call(this);

						var index = EnemyBullet.instances.indexOf(this);

						if (index >= 0) {
								EnemyBullet.instances.splice(index, 1);
						}
				}
		}]);

		return EnemyBullet;
}(GameObject);

EnemyBullet.instances = [];
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Enemy = function (_GameObject) {
	_inherits(Enemy, _GameObject);

	function Enemy() {
		_classCallCheck(this, Enemy);

		var _this = _possibleConstructorReturn(this, (Enemy.__proto__ || Object.getPrototypeOf(Enemy)).call(this));

		Enemy.instances.push(_this);

		_this.width = 64;
		_this.height = 64;
		_this.x = Math.floor(Math.random() * (Stage.instance.width - _this.width));
		_this.bottom = Stage.instance.top;

		_this.reloadTick = 0;
		_this.fireRate = 54;
		_this.canFire = true;

		_this.speed = 4;

		_this.value = 10;

		_this.sprite = new Sprite(_this, 'enemy.png');
		return _this;
	}

	_createClass(Enemy, [{
		key: 'update',
		value: function update(deltaTime) {
			_get(Enemy.prototype.__proto__ || Object.getPrototypeOf(Enemy.prototype), 'update', this).call(this, deltaTime);

			this.reload(deltaTime);
			this.fire(deltaTime);

			this.y += this.speed;

			this.checkCollisions();

			if (this.top >= Stage.instance.bottom) {
				this.destroy();
			}
		}
	}, {
		key: 'destroy',
		value: function destroy() {
			_get(Enemy.prototype.__proto__ || Object.getPrototypeOf(Enemy.prototype), 'destroy', this).call(this);

			var index = Enemy.instances.indexOf(this);

			if (index >= 0) {
				Enemy.instances.splice(index, 1);
			}
		}
	}, {
		key: 'reload',
		value: function reload(deltaTime) {
			if (this.canFire) return;

			++this.reloadTick;

			if (this.reloadTick >= this.fireRate) {
				this.reloadTick = 0;
				this.canFire = true;
			}
		}
	}, {
		key: 'fire',
		value: function fire(deltaTime) {
			if (!this.canFire) return;

			new EnemyBullet(this);
			this.canFire = false;
		}
	}, {
		key: 'checkCollisions',
		value: function checkCollisions() {
			for (var i = 0; i < Bullet.instances.length; ++i) {
				if (this.isColliding(Bullet.instances[i])) {
					Game.instance.score += this.value;

					this.destroy();
				}
			}
		}
	}]);

	return Enemy;
}(GameObject);

Enemy.instances = [];
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var EnemySpawn = function (_GameObject) {
	_inherits(EnemySpawn, _GameObject);

	function EnemySpawn() {
		_classCallCheck(this, EnemySpawn);

		var _this = _possibleConstructorReturn(this, (EnemySpawn.__proto__ || Object.getPrototypeOf(EnemySpawn)).call(this));

		EnemySpawn.instance = _this;

		_this.tickCount = 0;
		_this.spawnRate = 124;
		_this.tickTrigger = Math.ceil(Math.random() * _this.spawnRate);
		return _this;
	}

	_createClass(EnemySpawn, [{
		key: "update",
		value: function update(deltaTime) {
			++this.tickCount;

			if (this.tickCount >= this.tickTrigger) {
				new Enemy();

				this.tickCount = 0;
				this.tickTrigger = Math.ceil(Math.random() * this.spawnRate);
			}
		}
	}]);

	return EnemySpawn;
}(GameObject);
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Bullet = function (_GameObject) {
	_inherits(Bullet, _GameObject);

	function Bullet() {
		_classCallCheck(this, Bullet);

		var _this = _possibleConstructorReturn(this, (Bullet.__proto__ || Object.getPrototypeOf(Bullet)).call(this));

		Bullet.instances.push(_this);

		_this.velocity = 8;

		_this.width = 9;
		_this.height = 24;
		_this.x = Hero.instance.x + Hero.instance.width / 2 - _this.width / 2;
		_this.y = Hero.instance.y - _this.height + _this.velocity;

		_this.sprite = new Sprite(_this, 'bullet.png');
		return _this;
	}

	_createClass(Bullet, [{
		key: 'update',
		value: function update(deltaTime) {
			_get(Bullet.prototype.__proto__ || Object.getPrototypeOf(Bullet.prototype), 'update', this).call(this, deltaTime);

			this.y -= this.velocity;

			if (this.bottom < Stage.instance.top) {
				this.destroy();
			}
		}
	}, {
		key: 'destroy',
		value: function destroy() {
			_get(Bullet.prototype.__proto__ || Object.getPrototypeOf(Bullet.prototype), 'destroy', this).call(this);

			var index = Bullet.instances.indexOf(this);

			if (index >= 0) {
				Bullet.instances.splice(index, 1);
			}
		}
	}]);

	return Bullet;
}(GameObject);

Bullet.instances = [];
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Life = function (_GameObject) {
		_inherits(Life, _GameObject);

		function Life() {
				_classCallCheck(this, Life);

				var _this = _possibleConstructorReturn(this, (Life.__proto__ || Object.getPrototypeOf(Life)).call(this));

				Life.instances.push(_this);

				_this.width = 19;
				_this.height = 22;

				_this.x = Stage.instance.right - (_this.width + 16) * Life.instances.length;
				_this.y = 16;

				_this.sprite = new Sprite(_this, 'life.png');
				return _this;
		}

		_createClass(Life, [{
				key: 'destroy',
				value: function destroy() {
						_get(Life.prototype.__proto__ || Object.getPrototypeOf(Life.prototype), 'destroy', this).call(this);

						var index = Life.instances.indexOf(this);

						if (index >= 0) {
								Life.instances.splice(index, 1);
						}
				}
		}]);

		return Life;
}(GameObject);

Life.instances = [];
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Hero = function (_GameObject) {
	_inherits(Hero, _GameObject);

	function Hero() {
		_classCallCheck(this, Hero);

		var _this = _possibleConstructorReturn(this, (Hero.__proto__ || Object.getPrototypeOf(Hero)).call(this));

		Hero.instance = _this;

		_this.width = 64;
		_this.height = 64;
		_this.x = Stage.instance.width / 2 - _this.width / 2;
		_this.y = Stage.instance.height - _this.height - 32;

		_this.lives = 3;
		_this.respawnTick = 0;
		_this.respawnRate = 124;
		_this.deathless = false;
		_this.flickerTick = 0;
		_this.flickerRate = 16;

		_this.reloadTick = 0;
		_this.fireRate = 31;
		_this.canFire = true;

		_this.maxSpeed = 8;
		_this.acceleration = 0.5;
		_this.horizontalSpeed = 0;
		_this.verticalSpeed = 0;

		_this.sprite = new Sprite(_this, 'hero.png', {
			loop: false,
			ticksPerFrame: 6
		});
		return _this;
	}

	_createClass(Hero, [{
		key: 'update',
		value: function update(deltaTime) {
			_get(Hero.prototype.__proto__ || Object.getPrototypeOf(Hero.prototype), 'update', this).call(this, deltaTime);

			if (this.deathless) {
				this.flicker(deltaTime);
				this.respawn(deltaTime);
			}

			this.reload(deltaTime);
			this.fire(deltaTime);
			this.updateSpeed(deltaTime);
			this.updateAnims(deltaTime);
			this.move(deltaTime);
			this.decay(deltaTime);
			this.checkCollisions();
			this.checkBounds();
		}
	}, {
		key: 'destroy',
		value: function destroy() {
			if (this.deathless) return;

			--this.lives;

			if (this.lives < 0) {
				_get(Hero.prototype.__proto__ || Object.getPrototypeOf(Hero.prototype), 'destroy', this).call(this);

				Hero.instance = null;

				Game.instance.gameOver();
			} else {
				this.deathless = true;

				Game.instance.score -= 50;
			}
		}
	}, {
		key: 'flicker',
		value: function flicker(deltaTime) {
			++this.flickerTick;

			if (this.flickerTick >= this.flickerRate) {
				this.flickerTick = 0;

				if (this.sprite.opacity === 1) {
					this.sprite.opacity = 0.5;
				} else {
					this.sprite.opacity = 1;
				}
			}
		}
	}, {
		key: 'respawn',
		value: function respawn(deltaTime) {
			++this.respawnTick;

			if (this.respawnTick >= this.respawnRate) {
				this.respawnTick = 0;

				this.deathless = false;
				this.sprite.opacity = 1;
			}
		}
	}, {
		key: 'reload',
		value: function reload(deltaTime) {
			if (this.canFire) return;

			++this.reloadTick;

			if (this.reloadTick >= this.fireRate) {
				this.reloadTick = 0;
				this.canFire = true;
			}
		}
	}, {
		key: 'fire',
		value: function fire(deltaTime) {
			if (!this.canFire) return;

			if (Controller.instance.isDown(' ') || Controller.instance.isDown('spacebar')) {
				new Bullet();
				this.canFire = false;
			}
		}
	}, {
		key: 'updateSpeed',
		value: function updateSpeed(deltaTime) {
			if (Controller.instance.isDown('w') && this.verticalSpeed > -this.maxSpeed) {
				this.verticalSpeed -= this.acceleration;
			}

			if (Controller.instance.isDown('a') && this.horizontalSpeed > -this.maxSpeed) {
				this.horizontalSpeed -= this.acceleration;
			}

			if (Controller.instance.isDown('s') && this.verticalSpeed < this.maxSpeed) {
				this.verticalSpeed += this.acceleration;
			}

			if (Controller.instance.isDown('d') && this.horizontalSpeed < this.maxSpeed) {
				this.horizontalSpeed += this.acceleration;
			}
		}
	}, {
		key: 'updateAnims',
		value: function updateAnims(deltaTime) {
			if (this.horizontalSpeed < 0 && this.sprite.animationIndex !== 1) {
				this.sprite.animationIndex = 1;
				this.sprite.frameIndex = 0;
				this.sprite.framesPerAnimation = 2;
				this.sprite.paused = false;
			} else if (this.horizontalSpeed > 0 && this.sprite.animationIndex !== 2) {
				this.sprite.animationIndex = 2;
				this.sprite.frameIndex = 0;
				this.sprite.framesPerAnimation = 2;
				this.sprite.paused = false;
			} else if (this.horizontalSpeed === 0) {
				this.sprite.animationIndex = 0;
				this.sprite.frameIndex = 0;
				this.sprite.framesPerAnimation = 1;
			}
		}
	}, {
		key: 'move',
		value: function move(deltaTime) {
			this.x += this.horizontalSpeed;
			this.y += this.verticalSpeed;
		}
	}, {
		key: 'decay',
		value: function decay(deltaTime) {
			if (!Controller.instance.isDown('w') && !Controller.instance.isDown('s')) {
				if (this.verticalSpeed > 0) {
					this.verticalSpeed -= this.acceleration;

					if (this.verticalSpeed < this.acceleration) {
						this.verticalSpeed = 0;
					}
				} else if (this.verticalSpeed < 0) {
					this.verticalSpeed += this.acceleration;

					if (this.verticalSpeed > this.acceleration) {
						this.verticalSpeed = 0;
					}
				}
			}

			if (!Controller.instance.isDown('a') && !Controller.instance.isDown('d')) {
				if (this.horizontalSpeed > 0) {
					this.horizontalSpeed -= this.acceleration;

					if (this.horizontalSpeed < this.acceleration) {
						this.horizontalSpeed = 0;
					}
				} else if (this.horizontalSpeed < 0) {
					this.horizontalSpeed += this.acceleration;

					if (this.horizontalSpeed > this.acceleration) {
						this.horizontalSpeed = 0;
					}
				}
			}
		}
	}, {
		key: 'checkCollisions',
		value: function checkCollisions() {
			for (var i = 0; i < Enemy.instances.length; ++i) {
				if (this.isColliding(Enemy.instances[i])) {
					Enemy.instances[i].destroy();
					this.destroy();
				}
			}

			for (var i = 0; i < EnemyBullet.instances.length; ++i) {
				if (this.isColliding(EnemyBullet.instances[i])) {
					this.destroy();
				}
			}
		}
	}, {
		key: 'checkBounds',
		value: function checkBounds() {
			if (this.top < Stage.instance.top) {
				this.verticalSpeed = 0;
				this.top = Stage.instance.top;
			}

			if (this.left < Stage.instance.left) {
				this.horizontalSpeed = 0;
				this.left = Stage.instance.left;
			}

			if (this.bottom > Stage.instance.bottom) {
				this.verticalSpeed = 0;
				this.bottom = Stage.instance.bottom;
			}

			if (this.right > Stage.instance.right) {
				this.horizontalSpeed = 0;
				this.right = Stage.instance.right;
			}
		}
	}, {
		key: 'lives',
		get: function get() {
			return this._lives;
		},
		set: function set(value) {
			for (var i = Life.instances.length - 1; i >= 0; --i) {
				Life.instances[i].destroy();
			}

			for (var i = 0; i < value; ++i) {
				new Life();
			}

			this._lives = value;
		}
	}]);

	return Hero;
}(GameObject);
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Stage = function () {
	function Stage() {
		_classCallCheck(this, Stage);

		Stage.instance = this;

		this.element = document.createElement('canvas');
		Game.instance.element.appendChild(this.element);

		this.context = this.element.getContext('2d');

		this.resize();

		document.addEventListener('resize', this.resize.bind(this));
	}

	_createClass(Stage, [{
		key: 'resize',
		value: function resize() {
			this.element.setAttribute('width', this.width);
			this.element.setAttribute('height', this.height);
		}
	}, {
		key: 'update',
		value: function update(deltaTime) {
			//
		}
	}, {
		key: 'redraw',
		value: function redraw(deltaTime) {
			this.context.clearRect(0, 0, this.width, this.height);
		}
	}, {
		key: 'width',
		get: function get() {
			return this.element.offsetWidth;
		}
	}, {
		key: 'height',
		get: function get() {
			return this.element.offsetHeight;
		}
	}, {
		key: 'top',
		get: function get() {
			return 0;
		}
	}, {
		key: 'left',
		get: function get() {
			return 0;
		}
	}, {
		key: 'bottom',
		get: function get() {
			return this.top + this.height;
		}
	}, {
		key: 'right',
		get: function get() {
			return this.left + this.width;
		}
	}]);

	return Stage;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Game = function () {
	function Game() {
		_classCallCheck(this, Game);

		Game.instance = this;

		this.element = document.createElement('game');
		document.body.appendChild(this.element);

		this.scoreBoard = document.createElement('score');
		this.element.appendChild(this.scoreBoard);

		this.lastUpdated = 0;

		this.score = 0;

		new Controller();
		new Stage();
		new StarField('stars_1.png', 1);
		new StarField('stars_2.png', 1.15);
		new Hero();

		new EnemySpawn();

		requestAnimationFrame(this.loop.bind(this));
	}

	_createClass(Game, [{
		key: 'loop',
		value: function loop(timestamp) {
			var deltaTime = timestamp - this.lastUpdated;

			if (!this.paused) {
				this.update(deltaTime);
				this.redraw(deltaTime);
			}

			this.lastUpdated = timestamp;

			requestAnimationFrame(this.loop.bind(this));
		}
	}, {
		key: 'update',
		value: function update(deltaTime) {
			Stage.instance.update(deltaTime);

			for (var i = 0; i < GameObject.instances.length; ++i) {
				GameObject.instances[i].update(deltaTime);
			}
		}
	}, {
		key: 'redraw',
		value: function redraw(deltaTime) {
			Stage.instance.redraw(deltaTime);

			for (var i = 0; i < GameObject.instances.length; ++i) {
				GameObject.instances[i].redraw(deltaTime);
			}
		}
	}, {
		key: 'gameOver',
		value: function gameOver() {
			this.gameOverScreen = document.createElement('gameover');
			this.gameOverTitle = document.createElement('h1');
			this.restartButton = document.createElement('button');

			this.element.appendChild(this.gameOverScreen);
			this.gameOverScreen.appendChild(this.gameOverTitle);
			this.gameOverScreen.appendChild(this.restartButton);

			this.gameOverTitle.innerText = 'Game Over!';
			this.restartButton.innerText = 'Restart';

			this.restartButton.addEventListener('click', this.restart.bind(this));
		}
	}, {
		key: 'restart',
		value: function restart() {
			this.score = 0;

			new Hero();

			for (var i = Enemy.instances.length - 1; i >= 0; i--) {
				Enemy.instances[i].destroy();
			}

			if (this.gameOverScreen.parentNode) {
				this.gameOverScreen.parentNode.removeChild(this.gameOverScreen);
			}
		}
	}, {
		key: 'score',
		get: function get() {
			return this._score;
		},
		set: function set(value) {
			this._score = value;

			this.scoreBoard.innerText = 'Score: ' + value;
		}
	}]);

	return Game;
}();

document.addEventListener('DOMContentLoaded', function () {
	new Game();
});

requestAnimationFrame = requestAnimationFrame || webkitRequestAnimationFrame || mozRequestAnimationFrame || function (callback) {
	setTimeout(callback, 1000 / 60);
};